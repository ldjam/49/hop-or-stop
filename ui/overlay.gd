extends CanvasLayer

func _ready():
	GameState.connect('movement_phase_started', self, 'update_button_states')
	GameState.connect('construction_phase_started', self, 'update_button_states')

func reset():
	GameState.reset_level()

func toggle_highlight_all(button_pressed):
	if button_pressed:
		GameState.start_highlight_all()
	else:
		GameState.stop_highlight_all()

func _input(event):
	if event.is_action_pressed("ui_accept"):
		if GameState.is_construction_phase():
			GameState.start_movement()
		elif GameState.is_movement_phase():
			GameState.start_construction()
	
	#if event.is_action_pressed("hightlight_all"):
	#	GameState.start_highlight_all()
	#elif event.is_action_released("hightlight_all"):
	#	GameState.stop_highlight_all()

func start_movement():
	GameState.start_movement()

func start_construction():
	GameState.start_construction()

func update_button_states():
	$StartConstructionButton.visible = !GameState.is_construction_phase()
	$StartMovementButton.visible = !GameState.is_movement_phase()
	$HighlightButton.visible = !GameState.is_movement_phase()
	$ResetButton.visible = !GameState.is_movement_phase()

func exit_level():
	GameState.exit_level()

func play_button_sound():
	Sounds.play_button_sound()
