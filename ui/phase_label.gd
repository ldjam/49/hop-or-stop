extends RichTextLabel

func _process(delta):
	if GameState.is_phase(GameState.GamePhase.CONSTRUCTION):
		text = 'Construction Phase'
	elif GameState.is_phase(GameState.GamePhase.MOVEMENT):
		text = 'Movement Phase'
