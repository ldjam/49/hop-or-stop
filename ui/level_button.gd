extends Button

export (PackedScene) var level_scene

func _ready():
	var level_path = level_scene.resource_path
	if !GameState.finished_levels.has(level_path):
		GameState.finished_levels[level_path] = false
	$Finished.visible = GameState.finished_levels[level_path]

func start_level():
	GameState.load_level(level_scene)

func button_down():
	Sounds.play_button_sound()
