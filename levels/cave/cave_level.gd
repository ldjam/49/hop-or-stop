extends Node2D

func instant_reset(body: Node):
	if !body.get('is_player'):
		return
	
	# without deferred, movable objects are not set to static mode
	GameState.call_deferred('start_construction')
