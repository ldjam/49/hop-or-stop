extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	GameState.connect('movement_phase_started', self, 'update_music_states')
	GameState.connect('construction_phase_started', self, 'update_music_states')
	GameState.connect('level_exited', self, 'update_music_states')
func update_music_states():
	for node in get_children():
		if node is AudioStreamPlayer:
			var player = node as AudioStreamPlayer
			player.stop()
	
	if GameState.is_construction_phase():
		$ConstructionPhase.play()
	elif GameState.is_movement_phase():
		$MovementPhase.play()
