extends Area2D

func body_entered(body: Node):
	if !body.get("is_player"):
		return
	GameState.call_deferred('start_construction')
	GameState.mark_level_finished()
	
	var ok_button = $ConfirmationDialog.get_ok()
	ok_button.text = 'Return to menu'
	ok_button.mouse_default_cursor_shape = Control.CURSOR_POINTING_HAND
	var cancel_button = $ConfirmationDialog.get_cancel()
	cancel_button.text = 'Keep constructing'
	cancel_button.mouse_default_cursor_shape = Control.CURSOR_POINTING_HAND
	$ConfirmationDialog.popup_centered()

func dialog_confirmed():
	GameState.finish_level()
