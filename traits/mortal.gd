extends Node2D

export var minimum_acceptable_distance: float

var last_position = Vector2(-1000, -1000)

func _ready():
	$Counter.visible = false
	GameState.connect('construction_phase_started', self, 'stop_dying')

func start_dying():
	$DeathTimer.start()
	$Counter.text = char(ord('①') + ceil($DeathTimer.time_left) - 1)
	$Counter.visible = true

func stop_dying():
	$DeathTimer.stop()
	$Counter.visible = false

func is_dying() -> bool:
	return !$DeathTimer.is_stopped()

func handle_death():
	$Counter.visible = false
	GameState.start_construction()

func check_activity():
	if !GameState.is_movement_phase():
		return

	var body = get_parent() as RigidBody2D
	var distance_moved = abs(body.position.x - last_position.x)
	if is_dying():
		$Counter.text = char(ord($Counter.text) - 1)
		if distance_moved >= minimum_acceptable_distance:
			stop_dying()
	elif distance_moved < minimum_acceptable_distance:
		start_dying()
	last_position = body.position
