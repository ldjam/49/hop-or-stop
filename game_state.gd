extends Node

signal construction_phase_started
signal movement_phase_started
signal start_highlight_all
signal stop_highlight_all
signal level_exited

enum GamePhase {
  CONSTRUCTION,
  MOVEMENT
}

export(GamePhase) var current_phase
var is_dragging_object = false

var finished_levels = {} # will be filled by LevelButton
var current_level: PackedScene
var highlight_all: bool
var did_show_tutorial = false

func set_is_dragging_object(is_dragging: bool):
	if is_dragging:
		Input.set_default_cursor_shape(Input.CURSOR_DRAG)
	else:
		Input.set_default_cursor_shape(Input.CURSOR_ARROW)
	
	is_dragging_object = is_dragging

func get_is_dragging_object() -> bool:
	return is_dragging_object

func is_phase(phase) -> bool:
	return current_phase == phase

func is_construction_phase() -> bool:
	return is_phase(GamePhase.CONSTRUCTION)
	
func is_movement_phase() -> bool:
	return is_phase(GamePhase.MOVEMENT)

func start_highlight_all():
	highlight_all = true
	emit_signal('start_highlight_all')
	
func stop_highlight_all():
	highlight_all = false
	emit_signal('stop_highlight_all')
	
func start_movement():
	if is_dragging_object:
		return
		
	GameState.current_phase = GameState.GamePhase.MOVEMENT
	emit_signal('movement_phase_started')

func start_construction():
	if !did_show_tutorial:
		var modal = load("res://tutorial/tutorial.tscn").instance()
		get_tree().root.add_child(modal)
		did_show_tutorial = true
	
	GameState.current_phase = GameState.GamePhase.CONSTRUCTION
	emit_signal('construction_phase_started')

func load_level(level_scene: PackedScene):
	get_tree().change_scene_to(level_scene)
	current_level = level_scene
	call_deferred('start_construction')

func reset_level():
	load_level(current_level)

func save_transform(node: Node2D):
	node.set_meta('saved_transform', node.transform)

func restore_transform(node: Node2D):
	if !node.has_meta('saved_transform'):
		return
	var saved_transform = node.get_meta('saved_transform')
	node.call_deferred('set_transform', saved_transform)

func exit_level():
	current_phase = null
	get_tree().change_scene('res://game.tscn')
	emit_signal('level_exited')

func mark_level_finished():
	finished_levels[current_level.resource_path] = true

func finish_level():
	for is_finished in finished_levels.values():
		if !is_finished:
			exit_level()
			return
	get_tree().change_scene("res://end.tscn")
