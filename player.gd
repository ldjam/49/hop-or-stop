extends RigidBody2D

export var is_player = true
export var speed = 200
var should_jump = false

onready var animated_sprite = $AnimatedSprite

func _ready():
	GameState.save_transform(self)
	GameState.connect('construction_phase_started', self, 'handle_start_construction')
	GameState.connect('movement_phase_started', self, 'handle_start_movement')

func _physics_process(_delta):
	move()
	jump()

func move():
	var should_move = GameState.is_phase(GameState.GamePhase.MOVEMENT)
	if should_move && $Feet.get_overlapping_bodies().size() > 0 && linear_velocity.x <= 75:
		apply_central_impulse(Vector2(speed, 0))
	
func _input(event):
	if event.is_action_pressed("ui_up") && ($ExtendedFeet.get_overlapping_bodies().size() > 0 || $Feet.get_overlapping_bodies().size() > 0):
		should_jump = true

func jump():
	if should_jump && $Feet.get_overlapping_bodies().size() > 0:
		apply_central_impulse(Vector2(0, -800))
		Sounds.play_jump_sound()
		should_jump = false

func handle_start_movement():
	$MovementCamera.current=true
	should_jump=false
	mode = RigidBody2D.MODE_CHARACTER
	animated_sprite.play("walk")

func handle_start_construction():
	$MovementCamera.current=false
	should_jump=false
	mode = RigidBody2D.MODE_STATIC
	GameState.restore_transform(self)
	animated_sprite.play("idle")
