extends Node2D

enum RotationDirection {
  LEFT,
  RIGHT
}

# Configurations
var keyboard_rotation_speed = PI/2
var mouse_max_rotation_speed = PI
var mouse_rotation_max_distance = 200
var blocked_color = Color("#c91212")
var highlight_color = Color("#ffe600")
var highlight_cave_color = Color("#bc1fff")

# Variables
var is_selected = false
var block_selection = false
var wants_dropping = false
var is_hover = false

var is_mouse_rotating = false
var mouse_rotation_origin = null

var rotation_direction = null

var current_mouse_offset = null
var default_color = null

# Called when the node enters the scene tree for the first time.
func _ready():
	if get_body().has_node('CollisionPolygon2D'):
		var collision_polygon = get_body().get_node('CollisionPolygon2D') as CollisionPolygon2D
		$Area2D/CollisionPolygon2D.polygon = collision_polygon.polygon
		$Area2D/CollisionPolygon2D.position = collision_polygon.position
		$Area2D/CollisionPolygon2D.disabled = false
	else:
		var collision_shape = get_body().get_node('CollisionShape2D') as CollisionShape2D
		$Area2D/CollisionShape2D.shape = collision_shape.shape
		$Area2D/CollisionShape2D.position = collision_shape.position
		$Area2D/CollisionShape2D.disabled = false
	
	default_color = get_sprite().modulate 
	GameState.connect('construction_phase_started', self, 'handle_start_construction')
	GameState.connect('movement_phase_started', self, 'handle_start_movement')
	GameState.connect('start_highlight_all', self, 'start_highlight')
	GameState.connect('stop_highlight_all', self, 'stop_highlight')
	get_body().connect('input_event', self, 'handle_click_event')
	get_body().connect('mouse_entered', self, 'handle_mouse_enter')
	get_body().connect('mouse_exited', self, 'handle_mouse_exit')

func _process(delta):	
	if !is_selected:
		return
	
	if !is_mouse_rotating:	
		get_body().global_transform.origin = get_global_mouse_position() + current_mouse_offset
	else:
		var mouse_distance = get_global_mouse_position().x - mouse_rotation_origin.x
				
		if mouse_distance > 0:
			mouse_distance = min(mouse_distance, mouse_rotation_max_distance)
		else:
			mouse_distance = max(mouse_distance, -mouse_rotation_max_distance)
			
		get_body().global_rotation += delta * (mouse_distance / mouse_rotation_max_distance) * mouse_max_rotation_speed
	
	if rotation_direction == RotationDirection.LEFT:
		get_body().global_rotation += delta * -keyboard_rotation_speed
	elif rotation_direction == RotationDirection.RIGHT:
		get_body().global_rotation += delta * keyboard_rotation_speed
		
	if !can_be_dropped():
		get_sprite().modulate = blocked_color
	else:
		get_sprite().modulate = get_hightlight_color()
	
	if wants_dropping:
		toggle_selection()

func _input(event):
	block_selection = false
	
	if !is_selected:
		return
		
	if event.is_action("object_select"):
		rotation_direction = null
	
	if event.is_action_pressed("object_rotate") && !wants_dropping:
		start_mouse_rotation()
	elif event.is_action_released("object_rotate"):
		stop_mouse_rotation()
		
	# Needed because the item might rotate away under the cursor 
	# so we can't rely on the rigidbody being clicked
	if event.is_action_released("object_select") && !is_mouse_rotating:
		toggle_selection()
		# Prevents that the object isn't just selected again right away
		block_selection = true
			
	if event.is_action("object_rotate_left") && rotation_direction == null:
		rotation_direction = RotationDirection.LEFT
	elif event.is_action_released("object_rotate_left") && rotation_direction == RotationDirection.LEFT:
		rotation_direction = null
	elif event.is_action("object_rotate_right") && rotation_direction == null:
		rotation_direction = RotationDirection.RIGHT
	elif event.is_action_released("object_rotate_right") && rotation_direction == RotationDirection.RIGHT:
		rotation_direction = null
	
func handle_click_event(viewport, event, shape_idx):
	if event is InputEventMouseButton:
		if !is_selected && !block_selection && event.is_action_pressed("object_select"):
			toggle_selection()
		elif !is_selected && !block_selection && event.is_action_pressed("object_rotate"):
			toggle_selection()
			start_mouse_rotation()
		
func toggle_selection():
	var should_be_select = !is_selected
	if should_be_select && !GameState.get_is_dragging_object():
		current_mouse_offset = get_body().global_transform.origin - get_global_mouse_position()
		is_selected = should_be_select
		GameState.set_is_dragging_object(true)
		Sounds.play_drag_sound()
	elif !should_be_select && can_be_dropped():
		current_mouse_offset = null
		is_selected = should_be_select
		GameState.set_is_dragging_object(false)
		Sounds.play_drop_sound()
		wants_dropping = false
		stop_highlight()
	elif !should_be_select && !can_be_dropped():
		wants_dropping = true

func handle_mouse_enter():
	if !GameState.is_construction_phase():
		return
	if GameState.get_is_dragging_object():
		return
	if get_sprite().modulate == get_hightlight_color():
		return
	is_hover = true
	start_highlight()

func handle_mouse_exit():
	if !is_hover:
		return
	is_hover = false
	stop_highlight()
		
func start_highlight():
	get_sprite().modulate = get_hightlight_color() 

func stop_highlight():
	if !is_selected && !GameState.highlight_all:
		get_sprite().modulate = default_color
		
func start_mouse_rotation():
	is_mouse_rotating = true
	mouse_rotation_origin = get_global_mouse_position()

func stop_mouse_rotation():
	is_mouse_rotating = false
	mouse_rotation_origin = null
	toggle_selection()

func can_be_dropped() -> bool:
	# Compare to 1 as there's always the rigidbody's collider overlapping
	return $Area2D.get_overlapping_bodies().size() <= 1

func handle_start_construction():
	get_body().mode = RigidBody2D.MODE_STATIC
	GameState.restore_transform(get_body())
	get_body().input_pickable = true

func handle_start_movement():
	GameState.save_transform(get_body())
	get_body().mode = RigidBody2D.MODE_RIGID
	get_body().input_pickable = false

func get_body() -> RigidBody2D:
	return get_parent() as RigidBody2D

func get_sprite() -> Sprite:
	return get_parent().get_node("Sprite") as Sprite
	
func get_hightlight_color():
	if GameState.current_level.resource_path != "res://levels/cave/cave_level.tscn":
		return highlight_color
	else:
		return highlight_cave_color
