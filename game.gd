extends Node2D

func _ready():
	$MuteButton.pressed = !AudioServer.is_bus_mute(0)

func toggle_mute(button_pressed):
	AudioServer.set_bus_mute(0, !button_pressed)
