extends Camera2D

func _ready():
	var player = get_parent()
	var exit_node = player.get_parent().get_node('Exit')
	limit_right = max(exit_node.position.x, get_viewport().size.x)
