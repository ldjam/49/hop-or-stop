extends Camera2D

export var speed = 10

# Called when the node enters the scene tree for the first time.
func _ready():
	position = Vector2.ZERO
	GameState.connect('movement_phase_started', self, 'handle_start_movement')
	GameState.connect('construction_phase_started', self, 'handle_start_construction')
	var exit_node = get_parent().get_node('Exit')
	limit_right = max(exit_node.position.x, get_viewport().size.x)
	
func _process(delta):
	if GameState.current_phase == GameState.GamePhase.CONSTRUCTION:
		current=true
	else: 
		current=false

func _input(event):
	if !GameState.is_construction_phase():
		return
	
	if event.is_action("camera_left"):
		position.x = clamp(position.x - speed, limit_left, limit_right)
	elif event.is_action("camera_right"):
		position.x = clamp(position.x + speed, limit_left, limit_right)
	elif event.is_action("camera_down"):
		position.y = clamp(position.y + speed, limit_top, limit_bottom)
	elif event.is_action("camera_up"):
		position.y = clamp(position.y - speed, limit_top, limit_bottom)

func handle_start_movement():
	current=false
	
func handle_start_construction():
	current=true
