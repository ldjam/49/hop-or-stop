extends Node

func play_button_sound():
	$Button.play()

func play_drag_sound():
	$Drag.play()

func play_drop_sound():
	$Drop.play()

func play_jump_sound():
	$Jump.play()
