extends CanvasLayer

var text = [
	"""After a long day, you just want to get home. As usual, though, your path is riddled with deep chasms, tall cliffs, and bottomless pits. Luckily, there's also plenty of stuff lying around that for whatever reason, nobody seems to need.

Don't ask why, just use it!""",

	"""The game starts in the construction phase. Here, you can drag objects around, using the [left mouse button] or rotate them with the [right mouse button].
Don't know what can be dragged? Just hit the [Highlight Objects] button in the upper left corner.""",

	""" If needed, you can move the camera with [A] and [w]. 
Once you placed all the objects so that they just might get you to the exit, click [Start Movement].""",

	"""In the movement phase, you'll move automatically. If there's a small gap or the architectural masterpiece you just created happens to collapse, you can always try to leap to safety by pressing [Space].
	
	Good luck! """

]

func _ready():
	continue_tutorial()

func continue_tutorial():
	if text.empty():
		finish_tutorial()
		return

	$RichTextLabel.bbcode_text = text.pop_front()
	if text.empty():
		$EnoughButton.hide()
		$ContinueButton.text = "Let's go"

func finish_tutorial():
	queue_free()

